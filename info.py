#!/usr/bin/env python
import argparse
import subprocess

parser = argparse.ArgumentParser(description='Swiss army knife for OAM development. Get info about everything')
parser.add_argument('-f', '--fault', metavar='faultId', help='print description of faultId')
parser.add_argument('-r', '--revisions', metavar='packageId', help='print revisions of OAM components in BTS package')
args = vars(parser.parse_args())

if args['fault']:
    cmd = 'unzip -c helpers/faults/document_alarm_27.zip ama'+ args['fault']+'.man | less'
    subprocess.call(cmd, shell=True)
    exit(0)

if args['revisions']:
    cmd = './helpers/packageInfo.sh '+ args['revisions']
    subprocess.call(cmd, shell=True)
    exit(0)

