#!/usr/bin/env python
import os
import sys
import argparse
import tempfile
import subprocess
import ruamel.yaml

parser = argparse.ArgumentParser(description='Log Analyzer.')
parser.add_argument('logFile', metavar='file', nargs='+', help='log file to analyze')
parser.add_argument('-p', '--profile', help='profile')
parser.add_argument('-r', '--recipe', help='recipe')
parser.add_argument('-i', '--info', action='store_true', help='run \'info\' group')
parser.add_argument('-s', '--startup', action='store_true', help='run \'startup\' checks')
parser.add_argument('-f', '--faults', action='store_true', help='run \'faults\' checks')
parser.add_argument('-a', '--all', action='store_true', help='run \'all\' checks')
parser.add_argument('-l', '--pager', action='store_true', help='show results in pager')
parser.add_argument('-v', '--verbose', action='store_const', const='v', help='be verbose')
args = vars(parser.parse_args())
#print "args= ", args

# save file path before we change cwd
log = os.path.abspath(args['logFile'][0])
if log.split(".")[-1] == 'xz':
    logname = os.path.basename(args['logFile'][0]).split(".")[:-1]
    outlogname = "/tmp/" + ".".join(logname)
    cmd = "xz -d " + log + " --stdout >> " + outlogname
    os.system("rm -f " + outlogname)
    os.system(cmd)
    print "orig log=",log
    log = outlogname
print "log=",log

if os.path.islink(sys.argv[0]):
    os.chdir(os.path.dirname(os.readlink(sys.argv[0])) + "/")
else:
    os.chdir(os.path.dirname(sys.argv[0]) + "/")



class c:
    norm = '\x1b[0m'
    green = '\x1b[0;32;20m'
    red = '\x1b[0;31;20m'
    yellow = '\x1b[0;33;20m'
    bold = '\x1b[1m'
    coll40 = '\033[100D\033[40C'
    coll80 = '\033[100D\033[80C'



# Most used profiles are mapped to program options (-i, -s, -f)
# and 'faults' are default option (no flag needed to run this profile)
if not args['profile'] and not args['recipe']:
    option2profile = {
        'info':'profiles/info.yml',
        'startup':'profiles/startup.yml',
        'faults':'profiles/faults.yml'}
    # if one of above option is True in args: return profile file path
    prof = [option2profile[op] for op in option2profile if args[op]]
    ##print "prof= ", prof
    args['profile'] = prof[0] if len(prof) else 'profiles/all.yml'


recipes = {}
if (args['profile']):
    if (not os.path.isfile(args['profile'])):
        path_in_profiles_dir = os.path.join('profiles', args['profile'])
        if (not os.path.isfile(path_in_profiles_dir)):
            path_yml = path_in_profiles_dir + ".yml"
            if (not os.path.isfile(path_yml)):
                print "ERR: No profile '{}' found.".format(args['profile'])
                exit(1)
            else:
                args['profile'] = path_yml
        else:
            args['profile'] = path_in_profiles_dir

    ##print "profile param= ", args['profile']
    f=open(args['profile'], 'r')

    x = f.read()
    profile = ruamel.yaml.load(x, ruamel.yaml.RoundTripLoader)

    for section in profile:
        recipes[section] = []
        for entry in profile[section]:
            recipes[section].extend(subprocess.check_output(entry, shell=True).splitlines())
            recipes[section].sort()
    ##print "recipes= ", recipes

if args['recipe']:
    recipe = args['recipe']
    section = os.path.dirname(recipe).split('/')[-1]
    recipes[section] = [recipe]


out_file_path = ""
if args['pager']:
    out_file_path = tempfile.mkstemp()[1]
    out_file = open(out_file_path, 'w', 0)
    sys.stdout = out_file



if recipes.has_key('info'):
    for recipe in recipes['info']:
        cmd = '' + recipe + " " + log + " " + (args['verbose'] or "")
        subprocess.call(cmd, stdout=sys.stdout, shell=True)

devnull = open(os.devnull, 'w')
retval_to_print = {
        'startup': {
            0: '{c}[  OK  ]{n}'.format(c=c.green, n=c.norm),
            1: '{c}[FAILED]{n}'.format(c=c.red, n=c.norm),
            2: '{c}[NODATA]{n}'.format(c=c.yellow, n=c.norm)},
        'faults': {
            0: '{c}[FAILED]{n}'.format(c=c.red, n=c.norm),
            1: '{c}[  OK  ]{n}'.format(c=c.green, n=c.norm),
            2: '{c}[ WARN ]{n}'.format(c=c.yellow, n=c.norm),
            3: '[UNKNOW]'}
        }
if recipes.has_key('startup'):
    for recipe in recipes['startup']:
        cmd = 'bash {r} {l} {v}'.format(r=recipe, l=log, v=(args['verbose'] or ""))
        ret = subprocess.call(cmd, stderr=devnull, stdout=sys.stdout, shell=True)
        print c.coll80, retval_to_print['startup'][ret]

if recipes.has_key('faults'):
    for recipe in recipes['faults']:
        cmd = 'bash {r} {l} {v}'.format(r=recipe, l=log, v=(args['verbose'] or ""))
        print '{}{}{}'.format(c.bold, recipe, c.norm),
        ret = subprocess.call(cmd, stderr=devnull, stdout=sys.stdout, shell=True)
        print c.coll80, retval_to_print['faults'][ret]


if args['pager']:
    out_file.flush()
    subprocess.call('pager -rfK -FX ' + out_file_path, shell=True)

