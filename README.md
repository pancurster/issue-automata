### Usage:

```
usage: analyze.py [-h] [-p PROFILE] [-r RECIPE] [-i] [-s] [-a] [-l] [-v]
                  file [file ...]

Log Analyzer.

positional arguments:
  file                  log file to analyze

optional arguments:
  -h, --help            show this help message and exit
  -p PROFILE, --profile PROFILE
                        profile
  -r RECIPE, --recipe RECIPE
                        recipe
  -i, --info            run 'info' group
  -s, --startup         run 'startup' checks
  -a, --all             run 'all' checks
  -l, --pager           show results in pager
  -v, --verbose         be verbose
```

* ```./analyze path/to/SYSLOG155.log```
* ```./analyze -i recipe/info path/to/SYSLOG155.log``` - run only info recipes

### Dependencies
* grcat
* python-ruamel.yaml
* unzip

### Recipes:

* language
    * bash, perl, python, awk, any other linux standard allowed
* directories
    * recipes/faults - recipes for detecting error cases
    * recipes/info - recipes for pulling out various HW/SW info from log
    * recipes/startup - recipes for checking startup patterns
* convention
    * recipes returning:
        * 1 - considered OK
        * 0 - considered NOK

> this comes from default behavior of grep (0 - pattern found, 1 - not found) let you don't care
> about returning value from bash script (call to grep is enought)

