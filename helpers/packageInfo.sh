#!/bin/bash

fullPackageNum=$1

if [[ $fullPackageNum == CBTS00_OM_9999_COMMON* ]]; then
    repoPrefix="https://svne1.access.nsn.com/isource/svnroot"
    repoBranch="/BTS_D_SC_OAM_S17/tags"
    docFileURL="${repoPrefix}${repoBranch}/${fullPackageNum}/C_Application/SC_OAM/Doc/${fullPackageNum}_ReleaseNote.xml"
    docFileContent=$(svn cat $docFileURL | grep "^https" | awk -F'svnroot/' '{print $2}')
    echo "$docFileContent"
    exit 0
fi

declare -A PKG_TO_CB_REPO_DIR
PKG_TO_CB_REPO_DIR["CBTS00_FSM4_7000"]="CBTS00_FSM4_7000"
PKG_TO_CB_REPO_DIR["CBTS00_FSM3_7000"]="CBTS00_FSM3_7000"
PKG_TO_CB_REPO_DIR["CBTS00_FSM4_9999"]="CBTS_FSM4"
PKG_TO_CB_REPO_DIR["CBTS00_FSM3_9999"]="CBTS_FSM3"

pkgPrefix=$(awk -F'_' '{print $1"_"$2"_"$3}' <<< $fullPackageNum)
CbRepoDir=${PKG_TO_CB_REPO_DIR[$pkgPrefix]}

forOAM=$(svn pg svn:externals "https://svne1.access.nsn.com/isource/svnroot/BTS_SCM_CLOUD_CB/frozen/${CbRepoDir}/${fullPackageNum}/" | grep "for OAM")
cutedForOAM=$(echo ${forOAM} | cut -d '(' -f 2)

OAMrev=$(echo ${cutedForOAM} | cut -d '@' -f2 | cut -d ')' -f 1)

repoPrefix="https://svne1.access.nsn.com"
repoBranch=$(echo ${cutedForOAM} | cut -d '(' -f2 | cut -d '@' -f 1)
repoPostfix="/C_Application/SC_OAM/Doc"
repoURL=${repoPrefix}${repoBranch}${repoPostfix}"@"${OAMrev}
docFileName=$(svn ls ${repoURL})
docFileURL=${repoPrefix}${repoBranch}${repoPostfix}"/"${docFileName}"@"${OAMrev}
repoList=$(svn cat ${docFileURL}| grep -oP "^https?.*" | cut -d "]" -f1 | sed "s/https\:\/\/wrscmi\.inside\.nsn\.com\/isource\/svnroot\/BTS\_SC\_/\ /g" | sed "s/\/trunk/\ /g" | sed "s/\ \/meta/\ /g")

echo "${repoList}"
